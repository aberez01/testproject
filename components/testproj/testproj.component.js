angular
    .module("app")
    .component("testproj", {
        templateUrl: "components/testproj/testproj.html",
        controller: "testprojController",
        controllerAs: "proj"
    });