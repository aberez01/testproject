angular
    .module("app")
    .controller("testprojController", testprojController);

function testprojController(stat, real, testprojService, uiGridConstants) {
    var proj = this;
    proj.statValues = {
        id: 0,
        contHeadline: 1,
        imps: 2,
        clks: 3,
        ctr: 4,
        spend: 5,
        ecpc: 6,
        ecpm: 7
    };

    proj.realValues = {
        id: 0,
        sVisits: 1,
        vVisits: 2,
        clks: 3,
        converts: 4,
        revenue: 5,
        spend: 6,
        profit: 7,
        ctr: 8,
        cr: 9,
        cv: 10,
        roi: 11,
        epc: 12,
        epv: 13
    };

    proj.$onInit = function () {
        proj.statGrid = proj.createGridTable("stat");
        proj.statGrid.onRegisterApi = function (gridApi) {
            proj.gridStatApi = gridApi;
        };
        proj.realGrid = proj.createGridTable("real");
        proj.realGrid.onRegisterApi = function (gridApi) {
            proj.gridRealApi = gridApi;
        };
        real.get(function (data) {
            proj.realGrid.data = data.values;
        });
        stat.get(function (data) {
            proj.statGrid.data = data.values;
        });
    };

    proj.createGridTable = function (query) {
        return testprojService.setGridOptions(query);
    };

    proj.clearRealAll = function () {
        proj.gridRealApi.selection.clearSelectedRows();
    };

    proj.clearStatAll = function () {
        proj.gridStatApi.selection.clearSelectedRows();
    };

    proj.getRealVisibleRows = function () {
        for (var i = 0; i < proj.realGrid.paginationPageSize; i++) {
            proj.gridRealApi.selection.selectRowByVisibleIndex(i);
        }
    };

    proj.getStatVisibleRows = function () {
        for (var i = 0; i < proj.statGrid.paginationPageSize; i++) {
            proj.gridStatApi.selection.selectRowByVisibleIndex(i);
        }
    };
}