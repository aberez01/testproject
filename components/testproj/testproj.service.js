angular
    .module("app")
    .service("testprojService", testprojService);

function testprojService(uiGridConstants) {
    this.setGridOptions = function (query) {
        var statColumns = [
            {
                name: "ID",
                field: "0",
                cellTemplate: `<span>{{ COL_FIELD }}</span>`,
                enableFiltering: true,
                type: "number",
                sort: {
                    direction: uiGridConstants.ASC,
                    priority: 0
                }
            },
            {
                name: "Content Headline",
                field: "1",
                cellTemplate: `<span>{{ COL_FIELD }}</span>`,
                enableFiltering: false
            },
            {
                name: "Imps",
                field: "2",
                cellTemplate: `<span>{{ COL_FIELD }}</span>`,
                enableFiltering: false,
                type: "number"
            },
            {
                name: "Clks",
                field: "3",
                cellTemplate: `<span>{{ COL_FIELD }}</span>`,
                enableFiltering: false,
                type: "number"
            },
            {
                name: "CTR",
                field: "4",
                cellTemplate: `<span>{{ COL_FIELD | currency : "%"}}</span>`,
                enableFiltering: false,
                type: "number"
            },
            {
                name: "Spend",
                field: "5",
                cellTemplate: `<span>{{ COL_FIELD }}</span>`,
                enableFiltering: false,
                type: "number"
            },
            {
                name: "eCPC",
                field: "6",
                cellTemplate: `<span>{{ COL_FIELD }}</span>`,
                enableFiltering: false,
                type: "number"
            },
            {
                name: "eCPM",
                field: "7",
                cellTemplate: `<span>{{ COL_FIELD }}</span>`,
                enableFiltering: false,
                type: "number"
            }
        ];

        var realColumns = [
            {
                name: "ID",
                field: "0",
                cellTemplate: `<span>{{ COL_FIELD | number }}</span>`,
                type: "number",
                sort: {
                    direction: uiGridConstants.ASC,
                    priority: 0
                }
            },
            {
                name: "sVisits",
                field: "1",
                cellTemplate: `<span>{{ COL_FIELD }}</span>`,
                enableFiltering: false,
                type: "number"
            },
            {
                name: "vVisits",
                field: "2",
                cellTemplate: `<span>{{ COL_FIELD }}</span>`,
                enableFiltering: false,
                type: "number"
            },
            {
                name: "Cliks",
                field: "3",
                cellTemplate: `<span>{{ COL_FIELD }}</span>`,
                enableFiltering: false,
                type: "number"
            },
            {
                name: "Converts",
                field: "4",
                cellTemplate: `<span>{{ COL_FIELD }}</span>`,
                enableFiltering: false,
                type: "number"
            },
            {
                name: "Revenue",
                field: "5",
                cellTemplate: `<span>{{ COL_FIELD | currency : "$"}}</span>`,
                enableFiltering: false,
                type: "number"
            },
            {
                name: "Spend",
                field: "6",
                cellTemplate: `<span>{{ COL_FIELD | currency : "$"}}</span>`,
                enableFiltering: false,
                type: "number"
            },
            {
                name: "Profit",
                field: "7",
                cellTemplate: `<span>{{ row.entity[grid.appScope.proj.realValues.revenue] - row.entity[grid.appScope.proj.realValues.spend] | currency : "$"}}</span>`,
                enableFiltering: false,
                type: "number"
            },
            {
                name: "Ctr", field: "8",
                cellTemplate: `<span>{{ ((row.entity[grid.appScope.proj.realValues.clks]/row.entity[grid.appScope.proj.realValues.vVisits])*100 || 0.00) | currency : "%"}}</span>`,
                enableFiltering: false,
                type: "number"
            },
            {
                name: "Cr",
                field: "9",
                cellTemplate: `<span>{{ ((row.entity[grid.appScope.proj.realValues.converts]/row.entity[grid.appScope.proj.realValues.clks])*100 || 0.00) | currency : "%"}}</span>`,
                enableFiltering: false,
                type: "number"
            },
            {
                name: "Cv",
                field: "10",
                cellTemplate: `<span>{{ ((row.entity[grid.appScope.proj.realValues.converts]/row.entity[grid.appScope.proj.realValues.vVisits])*100 || 0.00) | currency : "%"}}</span>`,
                enableFiltering: false,
                type: "number"
            },
            {
                name: "Roi",
                field: "11",
                cellTemplate: `<span>{{ ((row.entity[grid.appScope.proj.realValues.profit]/row.entity[grid.appScope.proj.realValues.spend])*100 || 0.00) | currency : "%"}}</span>`,
                enableFiltering: false,
                type: "number"
            },
            {
                name: "Epc",
                field: "12",
                cellTemplate: `<span>{{ (row.entity[grid.appScope.proj.realValues.revenue]/row.entity[grid.appScope.proj.realValues.clks] || 0.00) | currency : "$"}}</span>`,
                enableFiltering: false,
                type: "number"
            },
            {
                name: "Epv",
                field: "13",
                cellTemplate: `<span>{{ (row.entity[grid.appScope.proj.realValues.revenue]/ row.entity[grid.appScope.proj.realValues.vVisits] || 0.00) | currency : "$"}}</span>`,
                enableFiltering: false,
                type: "number"
            }
        ];
        return {
            enableRowHeaderSelection: true,
            enableColumnMenus: false,
            enableColumnResizing: false,
            enableSorting: true,
            enableHorizontalScrollbar: 0,
            paginationPageSizes: [50, 100, 200, 500, 1000],
            paginationPageSize: 50,
            rowHeight: 80,
            enableFiltering: true,
            enableRowSelection: true,
            enableSelectAll: true,
            columnDefs: query === "stat" ? statColumns : realColumns
        };
    };
}
