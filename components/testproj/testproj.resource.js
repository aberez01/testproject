angular
    .module("app")
    .factory("stat", function($resource) {
        return $resource("components/testproj/data/AD-ID_stat.js");
    })
    .factory("real", function($resource) {
        return $resource("components/testproj/data/real_json.js");
    });